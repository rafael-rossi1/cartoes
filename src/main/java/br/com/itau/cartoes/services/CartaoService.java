package br.com.itau.cartoes.services;

import br.com.itau.cartoes.models.Cartao;
import br.com.itau.cartoes.models.Cliente;
import br.com.itau.cartoes.repositories.CartaoRepository;
import br.com.itau.cartoes.repositories.ClienteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.validation.Valid;
import java.util.Optional;

@Service
public class CartaoService {


    @Autowired
    private CartaoRepository cartaoRepository;

    public Cartao cadastrarCartao(Cartao cartao) {
        return cartaoRepository.save(cartao);
    }

    public Iterable<Cartao> buscarTodosCartoes() {
        return cartaoRepository.findAll();
    }

    public Cartao buscarCartaoPorID(int id) {
        Optional<Cartao> cartaoOptional = cartaoRepository.findById(id);
        if (cartaoOptional.isPresent()) {
            Cartao cartao = cartaoOptional.get();
            return cartao;
        } else {
            throw new RuntimeException("O cliente não foi encontrado");
        }
    }

    public Cartao atualizarCartao(int id, boolean ativo) {

        Cartao cartaoDB = buscarCartaoPorID(id);
        cartaoDB.setAtivo(ativo);
        return cartaoRepository.save(cartaoDB);
    }

    public void deletarCartao (int id) {
        if (cartaoRepository.existsById(id)) {
            cartaoRepository.deleteById(id);
        } else {
            throw new RuntimeException("Registro não existe");
        }
    }
}



