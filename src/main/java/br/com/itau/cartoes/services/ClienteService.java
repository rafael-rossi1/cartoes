package br.com.itau.cartoes.services;

import br.com.itau.cartoes.models.Cliente;
import br.com.itau.cartoes.repositories.ClienteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class ClienteService {


    @Autowired
    private ClienteRepository clienteRepository;

    public Cliente cadastrarCliente(Cliente cliente) {
        return clienteRepository.save(cliente);
    }

    public Iterable<Cliente> buscarTodosClientes() {
        return clienteRepository.findAll();
    }

    public Cliente buscarClientePorID(int id) {
        Optional<Cliente> clienteOptional = clienteRepository.findById(id);
        if (clienteOptional.isPresent()) {
            Cliente cliente = clienteOptional.get();
            return cliente;
        } else {
            throw new RuntimeException("O cliente não foi encontrado");
        }
    }

    public Cliente atualizarCliente(int id, Cliente cliente) {

        Cliente clienteDB = buscarClientePorID(id);
        cliente.setId(clienteDB.getId());
        return clienteRepository.save(cliente);
    }

    public void deletarCliente (int id) {
        if (clienteRepository.existsById(id)) {
            clienteRepository.deleteById(id);
        } else {
            throw new RuntimeException("Registro não existe");
        }
    }
}





