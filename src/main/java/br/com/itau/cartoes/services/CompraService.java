package br.com.itau.cartoes.services;

import br.com.itau.cartoes.models.Cliente;
import br.com.itau.cartoes.models.Compra;
import br.com.itau.cartoes.repositories.ClienteRepository;
import br.com.itau.cartoes.repositories.CompraRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

@Service
public class CompraService {

    @Autowired
    private CompraRepository compraRepository;

    public Compra cadastrarCompra(Compra compra) {
        return compraRepository.save(compra);
    }

    public Iterable<Compra> buscarTodasCompras() {
        return compraRepository.findAll();
    }
}