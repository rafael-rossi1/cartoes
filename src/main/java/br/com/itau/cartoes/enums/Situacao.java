package br.com.itau.cartoes.enums;

public enum Situacao {
        FATURA_ABERTA, FATURA_EM_ATRASO, PAGO

}
