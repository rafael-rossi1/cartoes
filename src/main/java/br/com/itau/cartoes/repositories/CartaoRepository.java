package br.com.itau.cartoes.repositories;

import br.com.itau.cartoes.models.Cartao;
import org.springframework.data.repository.CrudRepository;

public interface CartaoRepository extends CrudRepository<Cartao, Integer>  {
}
