package br.com.itau.cartoes.repositories;

import br.com.itau.cartoes.models.Compra;
import org.springframework.data.repository.CrudRepository;

public interface CompraRepository extends CrudRepository<Compra, Integer> {
}
