package br.com.itau.cartoes.controllers;


import br.com.itau.cartoes.models.Cartao;
import br.com.itau.cartoes.models.Cliente;
import br.com.itau.cartoes.services.CartaoService;
import br.com.itau.cartoes.services.ClienteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;

@RestController
@RequestMapping("/cartoes")
public class CartaoController {

    @Autowired
    private CartaoService cartaoService;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Cartao cadastrarCartao(@RequestBody @Valid Cartao cartao) {
        return cartaoService.cadastrarCartao(cartao);
    }

    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    public Iterable<Cartao> buscarTodosCartoes() {
        Iterable<Cartao> cartoes = cartaoService.buscarTodosCartoes();
        return cartoes;
    }

    @GetMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public Cartao buscarCartaoPorID(@PathVariable(name = "id") int id) {
        try {
            Cartao cartao = cartaoService.buscarCartaoPorID(id);
            return cartao;
        } catch (RuntimeException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }

    @PutMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public Cartao atualizarCartao(@RequestBody boolean ativo, @PathVariable(name = "id") int id) {
        try {
            Cartao cartaoAtualizado = cartaoService.atualizarCartao(id, ativo);
            return cartaoAtualizado;
        } catch (RuntimeException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deletarCartao(@PathVariable(name = "id") int id) {
        try {
            cartaoService.deletarCartao(id);
        } catch (RuntimeException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }
}

