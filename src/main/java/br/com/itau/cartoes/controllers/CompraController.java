package br.com.itau.cartoes.controllers;

import br.com.itau.cartoes.models.Cartao;
import br.com.itau.cartoes.models.Compra;
import br.com.itau.cartoes.services.CartaoService;
import br.com.itau.cartoes.services.CompraService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

    @RestController
    @RequestMapping("/compras")
    public class CompraController {

        @Autowired
        private CompraService compraService;

        @PostMapping
        @ResponseStatus(HttpStatus.CREATED)
        public Compra cadastrarCompra(@RequestBody @Valid Compra compra) {
            return compraService.cadastrarCompra(compra);
        }

        @GetMapping
        @ResponseStatus(HttpStatus.OK)
        public Iterable<Compra> buscarTodasCompras() {
            Iterable<Compra> compras = compraService.buscarTodasCompras();
            return compras;
        }
    }


